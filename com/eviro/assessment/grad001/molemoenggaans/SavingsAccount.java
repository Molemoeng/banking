package com.eviro.assessment.grad001.molemoenggaans;

import java.math.BigDecimal;

public class SavingsAccount extends Account {

    public SavingsAccount(int id, String accountNum, BigDecimal balance) {
        super(id, accountNum, balance);
    }

    @Override
    public void withDraw(BigDecimal amount){
        setBalance(getBalance().subtract(amount));
    }

    @Override
    public boolean canWithDraw(BigDecimal amount) {
        boolean isActive = getBalance().compareTo(BigDecimal.valueOf(1000)) >= 0;
        if(!isActive){
            return false;
        }

        int res = getBalance().compareTo(amount);

        return res >= 0;
    }


}
