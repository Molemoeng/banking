package com.eviro.assessment.grad001.molemoenggaans;

import java.math.BigDecimal;

public abstract class Account {
    private String customerNum;
    private int id;
    private String accountNum;
    private BigDecimal balance;

    public Account(int id, String accountNum, BigDecimal balance) {
        this.id = id;
        this.accountNum = accountNum;
        this.balance = balance;
    }

    public String getCustomerNum() {
        return customerNum;
    }

    public void setCustomerNum(String customerNum) {
        this.customerNum = customerNum;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public abstract void withDraw(BigDecimal amount);
    public abstract boolean canWithDraw(BigDecimal amount);
}
