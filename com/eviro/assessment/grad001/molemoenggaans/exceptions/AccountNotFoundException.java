package com.eviro.assessment.grad001.molemoenggaans.exceptions;

public class AccountNotFoundException extends Exception {

    public AccountNotFoundException(String s) {
        super(s);
    }
}
