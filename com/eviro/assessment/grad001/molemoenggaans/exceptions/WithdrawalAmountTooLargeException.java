package com.eviro.assessment.grad001.molemoenggaans.exceptions;

public class WithdrawalAmountTooLargeException extends Exception {

    public WithdrawalAmountTooLargeException(String s) {
        super(s);
    }
}
