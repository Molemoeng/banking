package com.eviro.assessment.grad001.molemoenggaans;

import java.math.BigDecimal;

public class CurrentAccount extends Account {

    private final BigDecimal overdraft;

    public CurrentAccount(int id, String accountNum, BigDecimal balance, BigDecimal overdraft) {
        super(id, accountNum, balance);
        this.overdraft = overdraft;
    }

    @Override
    public void withDraw(BigDecimal amount) {
        setBalance(getBalance().subtract(amount));
    }

    @Override
    public boolean canWithDraw(BigDecimal amount) {
        BigDecimal availableMoney = getBalance().add(overdraft);

        int res = availableMoney.compareTo(amount);

        return res >= 0;
    }


}
