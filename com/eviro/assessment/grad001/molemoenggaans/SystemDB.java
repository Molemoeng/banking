package com.eviro.assessment.grad001.molemoenggaans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public  class SystemDB {
   /** SavingsAccount savingsAccount = new SavingsAccount(101,1 ,2000);
    SavingsAccount savingsAccount1 = new SavingsAccount(102,2 ,5000);
    CurrentAccount currentAccount = new CurrentAccount(103,3,1000,10000);
    CurrentAccount currentAccount1 = new CurrentAccount(104,4,-5000,20000);
    **/
   private static SystemDB systemDB = null;

   private static List<Account> accounts = new ArrayList<>();

   private SystemDB(){

      accounts.add(new SavingsAccount(101, "1", BigDecimal.valueOf(2000)));
      accounts.add(new SavingsAccount(102, "2", BigDecimal.valueOf(5000)));
      accounts.add(new CurrentAccount(103, "3", BigDecimal.valueOf(1000),  BigDecimal.valueOf(10000)));
      accounts.add(new CurrentAccount(104, "4", BigDecimal.valueOf(-5000),  BigDecimal.valueOf(20000)));

   }

    public static SystemDB getInstance()
    {
        if (systemDB == null)
            systemDB = new SystemDB();
        return systemDB;
    }

    public static List<Account> getAccounts(){
       return accounts;
    }

    public static Account getAccount(String accountNum){
       Account account = null;

       for (Account acc : accounts){
           if(accountNum.equals(acc.getAccountNum())) {
               account = acc;
           }
       }

       return account;
    }

}
