package com.eviro.assessment.grad001.molemoenggaans;

import com.eviro.assessment.grad001.molemoenggaans.exceptions.AccountNotFoundException;
import com.eviro.assessment.grad001.molemoenggaans.exceptions.WithdrawalAmountTooLargeException;

import java.math.BigDecimal;

public class AccountRepo implements AccountService {

    @Override
    public void withdraw(String accountNum, BigDecimal amountToWithdraw) throws AccountNotFoundException,
            WithdrawalAmountTooLargeException {
        Account account = SystemDB.getAccount(accountNum);

        if(account == null)
            throw new AccountNotFoundException("Account number doesnt exist in our database.");

        if(!account.canWithDraw(amountToWithdraw))
            throw new WithdrawalAmountTooLargeException("You have reached the limit");

        //We can withdraw successfully
        account.withDraw(amountToWithdraw);

    }
}
