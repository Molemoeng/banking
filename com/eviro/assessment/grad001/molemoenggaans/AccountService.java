package com.eviro.assessment.grad001.molemoenggaans;

import com.eviro.assessment.grad001.molemoenggaans.exceptions.AccountNotFoundException;
import com.eviro.assessment.grad001.molemoenggaans.exceptions.WithdrawalAmountTooLargeException;

import java.math.BigDecimal;

public interface AccountService {
   void withdraw(String accountNum, BigDecimal amountToWithdraw) throws AccountNotFoundException, WithdrawalAmountTooLargeException;

}
