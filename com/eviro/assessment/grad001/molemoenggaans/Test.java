package com.eviro.assessment.grad001.molemoenggaans;

import com.eviro.assessment.grad001.molemoenggaans.exceptions.AccountNotFoundException;
import com.eviro.assessment.grad001.molemoenggaans.exceptions.WithdrawalAmountTooLargeException;

import java.math.BigDecimal;


public class Test{
    public static void main(String[] args) {
        SystemDB.getInstance();

        AccountService accountService = new AccountRepo();

        try {
            accountService.withdraw("4", BigDecimal.valueOf(15000));
            System.out.println( "Successfully withdrawn money.");
        } catch (AccountNotFoundException e) {
            System.out.println( "None exist account: " + e.getMessage());
        }catch (WithdrawalAmountTooLargeException e){
            System.out.println( "No enough funds: " + e.getMessage());
        }


    }


}
